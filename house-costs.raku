#!/usr/bin/env raku

use v6;

sub monthly_payments_with_suite(Num:D $monthly_payment) {

    say "\nThis property has a rental suite";
    say "\nTotal Monthly Payment with RI:";

    for (qw/1200 1500 1750 2000 2250/) -> $ri {

        my $monthly_payment_with_rental = $monthly_payment - $ri;
        my $tds = calculate-tds($monthly_payment_with_rental);
        printf "\t* rent=%d monthly_payments=%.2f tds=%.2f%% affordable=%s\n",
            $ri, $monthly_payment_with_rental, $tds, is-affordable($tds);
    }
}

sub calculate-tds ($total_monthly_payment) returns Num {
  my $gross-monthly-income = 16000;
  my $tds = ($total_monthly_payment / $gross-monthly-income) * 100;
  return $tds;
}

sub is-affordable ($tds) {
  if ($tds >= 30) {
    return False;
  }
  return True;
}

sub MAIN (
    Int:D :$list_price, #= Sale price on MLS
    Rat:D :$annual_property_tax, #= Annual propery tax cost
    Bool :$has_suite = False #= Does property have rental suite? (default no)
) {

    my $downpayment = 309_000;

    my Rat $transfer_tax = ($list_price - 200_000) * 0.02 + 2000;

    say "\nProperty Value: $list_price";
    say "Transfer Tax: $transfer_tax";

    my $mortgage_principal = $list_price - $downpayment;
    say "Mortage Principal: $mortgage_principal";

    my $annual_interest_rate_pc = 1.45;
    my $number_of_payments      = 300;

    my $monthly_interest_rate = ($annual_interest_rate_pc / 100) / 12;

    my $r1 = 1 + $monthly_interest_rate;

    my $monthly_mortgage_payment =
      $monthly_interest_rate *
      $mortgage_principal *
      $r1**$number_of_payments /
      ($r1**$number_of_payments - 1);

    printf "Monthly Mortgage Payments: %.2f\n", $monthly_mortgage_payment;

    my $monthly_property_tax = $annual_property_tax / 12;
    printf "Monthly Property Tax: %.2f\n", $monthly_property_tax;

    my $total_monthly_payment =
      $monthly_mortgage_payment + $monthly_property_tax;

    printf "Total monthly payment %.2f\n", $total_monthly_payment;
    my $tds = calculate-tds($total_monthly_payment);
    say "Is this property affordable?: " ~ is-affordable($tds);

    if ($has_suite) {
        monthly_payments_with_suite($total_monthly_payment)
    }
}
